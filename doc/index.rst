Account Costa Rica Module
#########################

Define an account chart template for Costa Rica. Based on the
trytonar-account-ar module for accounts in Argentina.

Usefull to create a localized account chart with the wizard in
Financial Management > Configuration > General Account >
Create Chart of Account from Template
