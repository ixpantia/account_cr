#This file is part of the account_cr module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from .account_cr import *


def register():
    Pool.register(
        PrintChartAccountStart,
        module='account_cr', type_='model')
    Pool.register(
        PrintChartAccount,
        module='account_cr', type_='wizard')
    Pool.register(
        ChartAccount,
        module='account_cr', type_='report')
